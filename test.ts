cy.fixture('urls.json').then((urls) => {
      const targetRedirectUrl = urls.targetRedirectUrl;

      getAllDownloadLinks()
        .each(($el) => {
          expect($el.attr('href'))
            .equal(targetRedirectUrl);
        });
    });
